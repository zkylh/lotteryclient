/**
 * !#zh 11选5走势图形态组件 (前一)
 * @information 期号，奇，偶，质，合，0路，1路，2路
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNums:{
            default:[],
            type:cc.Label
        },

        labIssue:{
            default:null,
            type:cc.Label
        },

        _data:null,
        _itemId:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            var decStr = ["奇","偶","质","合","0路","1路","2路"];
            this.color1 = new cc.Color(255, 255, 255);
            this.color2 = new cc.Color(124, 121, 126);
            this.node.color = this._data.color;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            for(var i=0;i<this._data.nums.length;i++)
            {
                if(this._data.nums[i] != 0)
                {
                    this.labNums[i].string = this._data.nums[i].toString();
                }
                else
                {
                    this.labNums[i].string = decStr[i];
                    this.labNums[i].node.color =  this.color1;
                    this.labNums[i].node.parent.getChildByName("spBg").active = true;
                }
            }
        }
    },

    /** 
    * 接收走势图形态信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },
    
    /** 
    * 刷新走势图形态信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this._data = data;
            var decStr = ["奇","偶","质","合","0路","1路","2路"];
            var color = new cc.Color(255, 255, 255);
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            for(var i=0;i<this._data.nums.length;i++)
            {
                if(this._data.nums[i] != 0)
                {
                    this.labNums[i].string = this._data.nums[i].toString();
                    this.labNums[i].node.color =  this.color2;
                    this.labNums[i].node.parent.getChildByName("spBg").active = false;
                }
                else
                {
                    this.labNums[i].string = decStr[i];
                    this.labNums[i].node.color =  this.color1;
                    this.labNums[i].node.parent.getChildByName("spBg").active = true;
                }
            }
        }
    },

    /** 
    * 接收走势图形态列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId = i;
    }
    
});
