/**
 * !#zh 快3走势图走势统计组件
 * @information 出现次数/平均遗漏/最大遗漏/最大连出 
 */
cc.Class({
    extends: cc.Component,

    properties: {
        spBgColor:{
            default:null,
            type:cc.Node
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        pfNumItem:{
            default:null,
            type:cc.Prefab
        },

        ndNumContent:{
            default:null,
            type:cc.Node
        },

        ndDec:{
            default:null,
            type:cc.Node
        },
        
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.spBgColor.color = this._data.color;
            this.labDec.string = this._data.dec;
            this.labDec.node.color = this._data.decColor;
            this.ndDec.color = this._data.decBgColor;
            for(var i=0;i<this._data.nums.length;i++)
            {
                var numItem = cc.instantiate(this.pfNumItem);
                var labNum = numItem.getChildByName("labNum");
                labNum.getComponent(cc.Label).string = this._data.nums[i];
                labNum.color = this._data.numColor;
                this.ndNumContent.addChild(numItem);
            }
        }
    },

    /** 
    * 接收走势图走势统计信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
