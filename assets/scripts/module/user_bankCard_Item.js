/**
 * !#zh 银行卡组件
 * @information 银行卡，卡号 
 */
cc.Class({
    extends: cc.Component,

    properties: {
        
        labBankName:{
            default: null,
            type: cc.Label
        },
         labBankLastNum:{
            default: null,
            type: cc.Label
        },
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labBankName.string = this._data.bankName;
            this.labBankLastNum.string = this._data.lastNum;
        }
    },

    /** 
    * 接收银行卡信息
    * @method init
    * @param {Object} ret
    */
    init: function(ret){
        this._data = ret;
    }

});
