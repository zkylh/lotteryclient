/**
 * 开奖中心
 */
cc.Class({
    extends: cc.Component,

    properties: {
        resultContent:{
            default: null,
            type: cc.Layout
        },
        
        publicReward:{
            default: null,
            type: cc.Prefab
        },

        ballsPrefab:{
            default: [],
            type: cc.Prefab
        },
        
        scrollview:{
            default:null,
            type:cc.ScrollView
        },

        betIsuseDetails:{
            default: null,
            type: cc.Prefab
        },

        labJackPot0:{
            default:null,
            type:cc.Label
        },

        labJackPot1:{
            default:null,
            type:cc.Label
        },

        _curPageIndex:1,
        _curLineIndex:11,
        _curPrefab:null,
        _isNowRefreshing:false,
        _listNode:[]//临时保存列表
    },

    // use this for initialization
    onLoad: function () {
        this._isNowRefreshing = false;
        this._curPageIndex = 1;
        this._curLineIndex = 11;
        this.labJackPot1.string = this.labJackPot0.string;
        this._listNode = [];
        this.initEventHandlers();
    },

    initEventHandlers:function(){
        window.Notification.on('rOpen_push',function(data){
            this.refreshOpen(data.data); 
        }.bind(this));

    },

    refreshOpen:function(data){
        var obj = Utils.findObjByArry(this._listNode,"lotterycode",data.LotteryCode.toString());
        if(obj != null)
        {
            var anData = this.getAnalysis(data.Number,data.LotteryCode,data.IsuseNum,data.OpenTime);
            obj.isusenum = data.IsuseNum;
            obj.ballFrames = anData.ballFrames;
            obj.ballNums = anData.ballNums;
            obj.isuse = anData.isuseTime;
            obj.msg = anData.msg
            obj.item.getComponent('open_publicOpen_item').updataData(obj);
        }
    },

    getIsuseAndTime:function(timeStr,isuse){
        var isuseAndWeekStr = "";
        if(timeStr != ""){
            var hourStr = "";
            var MinutesStr = "";
            var weekStr = "";

            var date = Utils.formateServiceDateStrToDate(timeStr);
            switch (date.getDay()) {
                case 0:weekStr="星期天";break;
                case 1:weekStr="星期一";break;
                case 2:weekStr="星期二";break;
                case 3:weekStr="星期三";break;
                case 4:weekStr="星期四";break;
                case 5:weekStr="星期五";break;
                case 6:weekStr="星期六";break;
                default:break;
            }
            hourStr = (date.getHours()).toString();
            hourStr = hourStr.length==1?"0"+hourStr:hourStr;

            MinutesStr = (date.getMinutes()).toString();
            MinutesStr = MinutesStr.length==1?"0"+MinutesStr:MinutesStr;

            isuseAndWeekStr = "第"+isuse+"期  "+hourStr+ " : " +MinutesStr+" ("+weekStr+")";
        }else{
            isuseAndWeekStr = "第"+isuse+"期";
        }
        return isuseAndWeekStr;
    },

    getAnalysis:function(number,loycode,isuseNum,opentime){
        var spriteNums = [];
        var Nums = [];
        var sumValue = 0;
        var stateStr = "";
        var isuseTime = "";
        var petNumberArray = number.split(" ");
        this.initPrefab( loycode);

        if(loycode == "101" || loycode == "102")
        {
            for(var j=0;j<petNumberArray.length;j++)
            {
                var value = parseInt(petNumberArray[j]);
                spriteNums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(value)); 
                sumValue += value;
            }
            sumValue = isNaN(sumValue)==true?0:sumValue;
            if(sumValue != 0){
                if(sumValue<=10){
                    stateStr = "小";
                }else{
                    stateStr = "大";
                }
                if(sumValue%2 == 0){
                    stateStr += "双";
                }else{
                    stateStr += "单";
                }   
            }
            stateStr = "和值："+ sumValue.toString() + "    形态：" + stateStr;
            var isuseTime = this.getIsuseAndTime(opentime,isuseNum);
            Nums = null;
        }
        else
        {
            spriteNums = null;
            for(var j=0;j<petNumberArray.length;j++)
            {
                Nums.push(petNumberArray[j]); 
            }
            var isuseTime = this.getIsuseAndTime(opentime,isuseNum);
        }

        var data = {
            isuseTime:isuseTime,
            ballFrames:spriteNums,
            ballNums:Nums,
            msg:stateStr
        }
        return data;
    },

    getResult:function(isrset){
        if(this.resultContent.node.childrenCount > 0 && !isrset)
            return;
        this.resultContent.node.removeAllChildren();
        var recv = function(ret){
            if(ret.Code !== 0)
            {
                ComponentsUtils.showTips(ret.Msg);
                cc.error(ret.Msg);
            }
            else
            {
                
                var data = ret.Data;
                for(var i = 0;i<data.length;i++)
                {
                    var anData = this.getAnalysis(data[i].Number,data[i].LotteryCode,data[i].IsuseNum,data[i].OpenTime);
                    var reward = cc.instantiate(this.publicReward);
                    reward.getComponent('open_publicOpen_item').init({
                        msg: anData.msg,
                        isuse: anData.isuseTime,
                        ballFrames:anData.ballFrames,
                        ballNums:anData.ballNums,
                        ballPrefab:this._curPrefab,
                        lotteryName: LotteryUtils.getLotteryTypeDecByLotteryId(data[i].LotteryCode)
                    });

                    var details = {
                        isusenum:data[i].IsuseNum,
                        isuse: anData.isuseTime,
                        ballFrames:anData.ballFrames,
                        ballNums:anData.ballNums,
                        lotterycode:data[i].LotteryCode.toString(),
                        lotteryName: LotteryUtils.getLotteryTypeDecByLotteryId(data[i].LotteryCode),
                        msg: anData.msg,
                        item:reward,
                        ballPrefab:this._curPrefab,
                    };
                    var clickEventHandler = new cc.Component.EventHandler();
                    clickEventHandler.target = this.node; 
                    clickEventHandler.component = "OpenUI"
                    clickEventHandler.handler = "onClickCallBack";
                    clickEventHandler.customEventData = data[i].LotteryCode.toString();
                    reward.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                    this.resultContent.node.addChild(reward); 
                    this._listNode.push(details);
                }
            }
            this._isNowRefreshing = false;
            ComponentsUtils.unblock();
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETHALLOPENLIST, data, recv.bind(this),"POST");   
        ComponentsUtils.block();
    },

    initPrefab:function(lotteryid){
        var type = lotteryid.toString().substr(0,1);
        this._curPrefab = null;
        var index = 0;
        switch (parseInt(type))
        {
            case 1://k3
            {
               index = 1;
            }
            break;
            case 2://115
            {  
                index = 2;
            }
            break;
            case 3://ssc
            {
                index = 3;
            }
            break;
            case 8://ssq
            {
                index = 4;
            }
            break;
            case 9://big
            {
                index = 5;
            }
            break;   
            default:
            break; 

        }
        this._curPrefab = this.ballsPrefab[index];
    },

    onClickCallBack:function(event, customEventData)
    {
        var obj = Utils.findObjByArry(this._listNode,"lotterycode",customEventData);
        if(obj == null)
            return;
            
        var recv = function(ret){
            if(ret.Code !== 0)
            {
                ComponentsUtils.showTips(ret.Msg);
                cc.error(ret.Msg);
            }
            else
            {
                var data = ret.Data;
                var page = cc.instantiate(this.betIsuseDetails);
                page.getComponent(this.betIsuseDetails.name).init({
                    ID:obj.lotterycode,
                    isuseData:obj,
                    revData:data,
                });
                var canvas = cc.find("Canvas");
                canvas.addChild(page);
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:obj.lotterycode,
            IsuseNum:obj.isusenum
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETTHELOTTERYISUSE, data, recv.bind(this),"POST");   
    },

    //下拉刷新
    scrollRefresh: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_TOP)
        {
          //  cc.log("openUI 订单列表：BOUNCE_TOP");
            //下拉刷新,当底部的offset抵达到某个点的时候
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(max_y - offset_y > 200){
              //  cc.log("openUI 订单列表：刷新：");
                this.updataData(true); 
            }
        }
    },

    updataData:function(isset){  
        if(this._isNowRefreshing == true)
            return;
        this._isNowRefreshing = true;
        this.getResult(isset);
    },

    onDestroy: function(){
        window.Notification.offType("rOpen_push");
    }

});
