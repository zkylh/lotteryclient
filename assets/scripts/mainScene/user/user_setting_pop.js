/**
 * 设置界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        severPrefab:{
            default: null,
            type: cc.Prefab
        },

        aboutPrefab:{
            default: null,
            type: cc.Prefab
        },

        tgVoice:cc.Toggle,
        _resultStr:null
        
    },

    // use this for initialization
    onLoad: function () {
        this.tgVoice.getComponent(cc.Toggle).isChecked = CL.SOUND.isHasEffect()==0?true:false;
        cc.loader.loadRes('/text/protoServe', function (error, result) {
            //cc.log('获取text',error || result);
            this._resultStr = result; 
        }.bind(this));
    },

    //声音
    onVoice: function(toggle, customEventData){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            CL.SOUND.set_effect_mute(false);
            ComponentsUtils.showTips("开启声音");
        }
        else
        {
            CL.SOUND.set_effect_mute(true);
            ComponentsUtils.showTips("关闭声音");
        }
    },

    //服务协议
    onServe: function(){
        var canvas = cc.find("Canvas");
        var severPrefab = cc.instantiate(this.severPrefab);
        //协议标题
        var title =severPrefab.getChildByName('content').getChildByName('content').getChildByName('labTopTitle');
        title.getComponent(cc.Label).string ='平台服务协议';
        var Scroll =severPrefab.getChildByName('content').getChildByName('content').getChildByName('ndBottomcontent').getChildByName('scrollview');
        var labDec =Scroll.getComponent(cc.ScrollView).content.getChildByName('labDec').getComponent(cc.Label);
        labDec.string =this._resultStr;
        canvas.addChild(severPrefab);
        severPrefab.active = true;
    },

    //关于
    onAbout: function(){
        var canvas = cc.find("Canvas");
        var aboutPrefab = cc.instantiate(this.aboutPrefab);
        canvas.addChild(aboutPrefab);
        aboutPrefab.active = true;
    },

//退出账号
    onOut: function(){
        var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0 )
            {
                User.setIsvermify(false);
                User.upDateUserInfo(true,"");
                User.setLoginToken(ret.Token);
                var callback = function callback(ret1) {
                  //  cc.log("login callback");
                    if(ret1 && ret1 == true)
                    {
                        window.Notification.emit("onGotoUser","");
                    }   
                    else
                        window.Notification.emit("onGotoHall","");
                }
                CL.NET.login();
                CL.MANAGECENTER.gotoLoginPop(callback.bind(this));
                this.onClose();
            }
            else
            {
                this.getOneToken();
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            Equipment:Utils.getEquipment(),
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.OUTACCOUNT, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

    //获取一个临时token
    getOneToken:function(){
        cc.log("user_setting_pop 获取一个临时token");
        var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0 )
            {
                User.setIsvermify(false);
                User.upDateUserInfo(true,"");
                User.setLoginToken(ret.Token);
                var callback = function callback(ret1) {
              //      cc.log("login callback");
                    if(ret1 && ret1 == true)
                    {
                        window.Notification.emit("onGotoUser","");
                    }   
                    else
                        window.Notification.emit("onGotoHall","");
                }
                CL.NET.login();
                CL.MANAGECENTER.gotoLoginPop(callback.bind(this));
                this.onClose();
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);

        var data = {
            Equipment:Utils.getEquipment(),
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETTOKEN, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
});
